name             'lsulibraries-drush'
maintainer       'YOUR_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures lsulibraries-drush'
long_description 'Installs/Configures lsulibraries-drush'
version          '0.1.0'

depends "drush", "~>0.10.0"
depends "apt"
