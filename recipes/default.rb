#
# Cookbook Name:: lsulibraries-drush
# Recipe:: default
#
# Copyright (C) 2015 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "apt" if node['platform_family'] == 'debian'
include_recipe "drush"
